'use strict';

var scene = new THREE.Scene();
var camera = new THREE.PerspectiveCamera(75, window.innerWidth / window.innerWidth, 0.1, 1000 );

var renderer = new THREE.WebGLRenderer();
renderer.setSize( window.innerWidth, window.innerHeight );
document.body.appendChild( renderer.domElement);

renderer.setClearColor(0x00000);

var geometry = new THREE.BoxGeometry(3, 5, 3, 1);
var material = new THREE.MeshPhongMaterial({ color: 0x44aa88 });
var cube = new THREE.Mesh( geometry, material);
var light = new THREE.DirectionalLight(0xffffff, 1);
light.position.set(-1, 2, 4);


scene.add(light);
scene.add(cube);
camera.position.z = 8;

 function animate(time) {
	time *= 0.001;
	cube.rotation.y = time; 
	cube.rotation.x = time; 
	renderer.render(scene, camera);
	requestAnimationFrame( animate );
}

animate();