'use strict';

var width = window.innerWidth,
	height = window.innerHeight,
	canvas = document.getElementById('canv');

canvas.setAttribute('width', width);
canvas.setAttribute('height', height);

var renderer = new THREE.WebGLRenderer({canvas: canvas});
renderer.setClearColor(0x000000);

var scene = new THREE.Scene();

var camera = new THREE.PerspectiveCamera(45, width / height, 0.1, 5000);
camera.position.set(0, 0, 1000);

var light = new THREE.AmbientLight(0xffffff);
scene.add(light);

var geometry = new THREE.SphereGeometry(200, 30, 30);
var material = new THREE.MeshBasicMaterial({color: 0xffffff, vertexColors: THREE.FaceColors}); // wareFrame: true 
for(var i = 0; i < geometry.faces.length; i++){
	geometry.faces[i].color.setRGB(Math.random(), Math.random(), Math.random());
}
var mesh = new THREE.Mesh(geometry, material);
scene.add(mesh);
var loop = ()=>{
	mesh.rotation.y += Math.PI/500;
	renderer.render(scene, camera);
	requestAnimationFrame(()=>{
		loop();
	});
}
loop();