'use strict';

var scene = new THREE.Scene();
var camera = new THREE.PerspectiveCamera(65, window.innerWidth / window.innerWidth, 0.1, 1000 );

var renderer = new THREE.WebGLRenderer({antialias: true});
renderer.setPixelRatio( window.devicePixelRatio );
var objLoader = new THREE.OBJLoader2();

objLoader.loadMtl('/model/building_04.mtl', null, (materials) => {
	objLoader.setMaterials(materials);
	objLoader.load('../model/building_04.obj', (event) => {
		var root = event.detail.loaderRootNode;
		scene.add(root);
	});
});

renderer.setSize( window.innerWidth, window.innerHeight );
var dom = document.body.appendChild( renderer.domElement);
var control = new THREE.OrbitControls(camera, dom);
control.target.set(0, 5, 0);


camera.position.y += 5;
camera.position.z += 12;
var light = new THREE.DirectionalLight('#edfc60', 2);
light.position.set(-2, 2, 4);
light.position.set(-10, 10, 4);


scene.add(light);

 function animate() {
	renderer.render(scene, camera);
	requestAnimationFrame( animate );
	control.update();
}

animate();