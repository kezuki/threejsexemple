'use strict';

var scene = new THREE.Scene(),
	camera = new THREE.PerspectiveCamera(65, window.innerWidth / window.innerHeight, 0.1, 10000),
	render = new THREE.WebGLRenderer({antialias: true});
render.setSize(window.innerWidth, window.innerHeight);
render.setClearColor(0xFFFFFF);
document.body.appendChild(render.domElement)
camera.position.z = 190;

var manager = new THREE.LoadingManager(),
	loader = new THREE.ImageLoader(manager),
	light = new THREE.DirectionalLight(0xfff7e8, 1),
	amColor = '#faffe3',
	amLight = new THREE.AmbientLight(amColor),
	controls = new THREE.TrackballControls( camera );
	
var	textureBody = new THREE.Texture(),
	textureHead = new THREE.Texture();

loader.load( '/model/Body diff MAP.jpg', function (image) {
	textureBody.image = image;
	textureBody.neeUpdate = true;
});
loader.load( '/model/HEAD diff MAP.jpg', function (image) {
	textureHead.image = image;
	textureHead.neeUpdate = true;
});

var meshes = [],
	objLoader = new THREE.OBJLoader();

objLoader.load('../model/bb8.obj', function (object) {
	console.log(object);
	object.traverse((child) => {
		if (child instanceof THREE.Mesh){
			meshes.push(child)
		}
	});

	var head = meshes[0],
		body = meshes[1],
		bumpMapBody = new THREE.TextureLoader().load('/model/BODY bump MAP.jpg'),
		bumpMapHeader = new THREE.TextureLoader().load('/model/HEAD bump MAP.jpg');

	head.material = new THREE.MeshPhongMaterial( {
		map: textureHead,
		specular: 0xfceed2,
		bumpScale: 0.4, 
		shininess: 25		
		
	})
	console.log('ss', head.material.map);
	body.material = new THREE.MeshPhongMaterial({
		map: textureBody,
		specular: 0xfceed2,
		bumpScale: 0.4, 
		shininess: 25	
	})
	
	head.position.y = -80;
	body.position.y = -80;

	scene.add(head);
	scene.add(body);


});
scene.add(amLight);
scene.add(light);

var rendering = function () {
	requestAnimationFrame(rendering);
	controls.update();
	render.render(scene, camera);
};

rendering();
	